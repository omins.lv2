#!/usr/bin/env python

import os
import shutil
import subprocess

from waflib.extras import autowaf

# Version of this package (even if built as a child)
OMINS_VERSION = '0.0.0'

# Variables for 'waf dist'
APPNAME = 'omins.lv2'
VERSION = OMINS_VERSION

# Mandatory variables
top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_c')
    opt.load('lv2')

def configure(conf):
    conf.load('compiler_c', cache=True)
    conf.load('lv2', cache=True)
    conf.load('autowaf', cache=True)
    autowaf.set_c_lang(conf, 'c99')

    autowaf.check_pkg(conf, 'lv2', atleast_version='1.0.0', uselib_store='LV2')

    autowaf.check_function(conf, 'c',  'posix_memalign',
                           defines     = '_POSIX_C_SOURCE=200809L',
                           header_name = 'stdlib.h',
                           define_name = 'HAVE_POSIX_MEMALIGN',
                           mandatory   = False)

    # Set env.pluginlib_PATTERN
    pat = conf.env.cshlib_PATTERN
    if pat[0:3] == 'lib':
        pat = pat[3:]
    conf.env.pluginlib_PATTERN = pat
    conf.env.pluginlib_EXT = pat[pat.rfind('.'):]

    conf.run_env.append_unique('LV2_PATH', [conf.build_path('lv2')])
    autowaf.display_summary(conf, {'LV2 bundle directory': conf.env.LV2DIR})

def build_plugin(bld, lang, bundle, name, source, defines=None):
    # Build plugin library
    penv = bld.env.derive()
    penv.cshlib_PATTERN = bld.env.pluginlib_PATTERN
    lib = []
    if not bld.env.MSVC_COMPILER:
        lib = ['m']
    obj = bld(features     = '%s %sshlib' % (lang,lang),
              env          = penv,
              source       = source,
              includes     = ['.', 'src/include'],
              name         = name,
              target       = os.path.join('lv2', bundle, name),
              uselib       = ['LV2'],
              install_path = '${LV2DIR}/' + bundle,
              lib          = lib)
    if defines != None:
        obj.defines = defines

    # Install data file
    data_file = '%s.ttl' % name
    bld.install_files('${LV2DIR}/' + bundle, os.path.join(bundle, data_file))

def build(bld):
    # Copy data files to build bundle (build/omins.lv2)
    def do_copy(task):
        src = task.inputs[0].abspath()
        tgt = task.outputs[0].abspath()
        return shutil.copy(src, tgt)

    for i in bld.path.ant_glob('omins.lv2/*.ttl'):
        bld(features     = 'subst',
            is_copy      = True,
            source       = i,
            target       = 'lv2/omins.lv2/%s' % i.name,
            install_path = '${LV2DIR}/omins.lv2')

    bld(features     = 'subst',
        source       = 'omins.lv2/manifest.ttl.in',
        target       = bld.path.get_bld().make_node('lv2/omins.lv2/manifest.ttl'),
        LIB_EXT      = bld.env.pluginlib_EXT,
        install_path = '${LV2DIR}/omins.lv2')

    plugins = ['adenv',
               'adenv_lvl',
               'comparison',
               'dahdsr_fexp',
               'dahdsr_hexp',
               'fast_crossfade',
               'formant_filter',
               'hz_voct',
               'masher',
               'multiplexer',
               'prob_switch',
               'range_trans',
               'sample_and_hold',
               'signal_abs',
               'slew_limiter',
               'slide',
               'waveguide_mesh']

    for i in plugins:
        build_plugin(bld, 'c', 'omins.lv2', i,
                     ['src/%s.c' % i])
              
def lint(ctx):
    subprocess.call('cpplint.py --filter=+whitespace/comments,-whitespace/tab,-whitespace/braces,-whitespace/labels,-build/header_guard,-readability/casting,-readability/todo,-build/include src/* serd/*', shell=True)
    
